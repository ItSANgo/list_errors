/**
 * @file list_errors.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief 
 * @version 0.1.0
 * @date 2020-01-25
 * 
 * @copyright Copyright (c) 2020 Mitsutoshi Nakano
 * 
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sysexits.h>

void usage(int exit_status, FILE *output_fp, const char *command_name)
{
    fprintf(output_fp, "Usage: %s to_number from_number\n", command_name);
    exit(exit_status); /*NOTREACHED*/
}

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        usage(EX_USAGE, stderr, argv[0]); /*NOTREACHED*/
    }
    int first_errno = (argc > 2) ? atoi(argv[2]) : 0;
    int last_errno = atoi(argv[1]);
    int i;
    for (i = first_errno; i <= last_errno; ++i) {
        printf("%d:%s\n", i, strerror(i));
    }
    exit(EX_OK); /*NOTREACHED*/
}
